package com.example.worldcinema.common

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.worldcinema.R
import com.example.worldcinema.databinding.TrendsItemBinding

class TrendsAdapter(

    private val context : Context,
    private val trends : List<Int>


) : RecyclerView.Adapter<TrendsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater
            .from(context)
            .inflate(R.layout.trends_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = TrendsItemBinding.bind(holder.itemView)
        val trends = trends[position]
    }

    override fun getItemCount()= trends.size
    }
